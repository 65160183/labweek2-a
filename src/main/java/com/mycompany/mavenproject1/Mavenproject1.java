/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.mavenproject1;

import java.util.Scanner;

/**
 *
 * @author LENOVO
 */
public class Mavenproject1 {

    static char table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char turn = 'X';
    static int row, col;
    static boolean gameOver = false;
    static boolean reset = false;

    public static void main(String[] args) {
        System.out.println("Welcome to XO GAME");

        while (!reset) {
            while (!gameOver) {
                printTable();
                printplayer();
                inputrowcol();
                if (Checkwin()) {
                    printTable();
                    printWin();
                    gameOver = true;
                    break;
                } else if (CheckDraw()) {
                    printTable();
                    printDraw();
                    gameOver = true;
                    break;
                }
                switchPlayer();
            }
            printreset();
        }
    }

    private static void printTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }
    }

    private static void inputrowcol() {
        Scanner kb = new Scanner(System.in);
        while (true) {
            System.out.print("input row,col :");
            row = kb.nextInt();
            col = kb.nextInt();
            if (table[row - 1][col - 1] == '-') {
                table[row - 1][col - 1] = turn;
                break;
            } else {
                System.out.println("Pls input agin");
            }
        }
    }

    private static void printplayer() {
        System.out.println("Turn " + turn);
    }

    private static void switchPlayer() {
        if (turn == 'X') {
            turn = 'O';
        } else {
            turn = 'X';
        }
    }

    private static boolean Checkwin() {
        if (CheckRow()) {
            return true;
        }
        if (CheckCol()) {
            return true;
        }
        if (CheckDownleft()) {
            return true;
        }
        if (CheckDownright()) {
            return true;
        }
        return false;
    }

    private static void printWin() {
        System.out.println("Winner is " + turn + " !!!");
    }

    private static boolean CheckDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

    private static void printDraw() {
        System.out.println("Draw");
    }

    private static boolean CheckRow() {
        for (int i = 0; i < 3; i++) {
            if (table[row - 1][i] != turn) {
                return false;
            }
        }
        return true;
    }

    private static boolean CheckCol() {
        for (int j = 0; j < 3; j++) {
            if (table[j][col - 1] != turn) {
                return false;
            }
        }
        return true;
    }

    private static boolean CheckDownleft() {
        for (int i = 0; i < 3; i++) {
            if (table[i][i] != turn) {
                return false;
            }
        }
        return true;
    }

    private static boolean CheckDownright() {
        for (int i = 0; i < 3; i++) {
            if (table[i][2 - i] != turn) {
                return false;
            }
        }
        return true;
    }

    private static void printreset() {
        Scanner kb = new Scanner(System.in);
        System.out.println("Reset Game XO? (Y/N)");
        String ans = kb.next().toUpperCase();
        if (ans.equals("Y")) {
            reset = false;
            gameOver = false;
            turn = 'X';
            table = new char[][]{{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
        } else {
            reset = true;
            System.out.println("Thank you for playing!!");
            System.exit(0);
        }
    }
}
